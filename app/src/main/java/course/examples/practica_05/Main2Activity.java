package course.examples.practica_05;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        Intent intent = getIntent();
        String message = intent.getStringExtra("Practica5");
        ImageView image;
        switch(message){
            case "1":
                image = (ImageView)findViewById(R.id.imageView1);
                image.setImageResource(R.mipmap.apple);
                break;

            case "2":
                image = (ImageView)findViewById(R.id.imageView1);
                image.setImageResource(R.mipmap.pineapple);
                break;

            case "3":
                image = (ImageView)findViewById(R.id.imageView1);
                image.setImageResource(R.mipmap.watermelon);
                break;

            case "4":
                image = (ImageView)findViewById(R.id.imageView1);
                image.setImageResource(R.mipmap.grape);
                break;

        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        

    }

}
